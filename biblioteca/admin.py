from django.contrib import admin
from biblioteca.models import Editor, Autor, Libro

class AutorAdmin(admin.ModelAdmin):
	#Esta variable es para mostrar esos datos en la barra lateral del panel de administración
	list_display = ("nombre", "apellidos","email")
	#Esta variable sirve para buscar objetos basandose en los atributos 
	search_fields = ("nombre", "apellidos")

class EditorAdmin(admin.ModelAdmin):
	#Esta variable es para mostrar esos datos en la barra lateral del panel de administración
	list_display = ("nombre", "website")
	#Esta variable sirve para buscar objetos basandose en los atributos 
	search_fields = ("nombre",)

class LibroAdmin(admin.ModelAdmin):
	#Esta variable es para mostrar esos datos en la barra lateral del panel de administración
	list_display = ("titulo","fecha_publicacion")
	#Esta variable sirve para buscar objetos basandose en los atributos 
	search_fields = ("titulo",)
	#Esta variable sirve para poder filtrar los datos
	list_filter = ("fecha_publicacion",)
	date_hierarchy = 'fecha_publicacion'
	#Sirve para ordenar por fecha de publicación en orden descendente (-)
	ordering = ('-fecha_publicacion',)	
	#sirve para ordenar la manera en que se agregan los datos 
	# en los formularios se puedden excluir algunos datos si se quiere
	# fields = ('titulo', 'autores', 'editor', 'portada')

	#filtro que sirve para agregar los autores(ManyToManyField) de forma mas interactiva 
	filter_horizontal = ("autores",)
	#filtro que sirve para agregar en el formulario de la clave foranea buscarlo
	# y seleccionarlo por su id
	raw_id_fields = ('editor',)


admin.site.register(Editor,EditorAdmin)
admin.site.register(Autor,AutorAdmin)
admin.site.register(Libro,LibroAdmin)