from django.template.loader import get_template
from django.http import HttpResponse, Http404
from django.template import Context
from django.shortcuts import render
import datetime

def hola(request):
	#Antes de usar shortcuts
	# t = get_template("Hola.html")
	# c = Context({"nombre" : "Isaac"})
	# html = t.render(c)

	##Despues de usar shortcuts
	return render(request, "Hola.html", {"nombre":"Isaac"})

##Vista para la fecha actual del sistema
def fechaActual(request):
	#Antes
	# ahora = datetime.datetime.now()
	# t = get_template('FechaActual.html')
	# html = t.render(Context({'fechaActual': ahora}))
	# return HttpResponse(html)

	#despues
	ahora = datetime.datetime.now()
	return render(request, 'fechaActual.html', {'fechaActual': ahora})


def horasAdelante(request,horas):
	try:
		offset = int(horas)
	except ValueError:
		raise Http404()
	dt= datetime.datetime.now()+datetime.timedelta(hours = offset)
	return render(request,"horasAdelante.html",{"horasAdelante":dt})
	# html = """
	# <html>
	# 	<body>
	# 		<h1>En %s hora(s), seran:</h1> 
	# 		<h3>%s</h3>
	# 	</body>
	# </html>""" % (horas, dt)
	# return HttpResponse(html)

# def atributosMeta(request):
# 	valor = request.META.items()
# 	valor.sort()
# 	html = []
# 	for k, v in valor:
# 		html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
# 	return HttpResponse('<table>%s</table>' % '\n'.join(html))